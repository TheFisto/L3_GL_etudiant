#include "Route.hpp"

bool Route::operator==(const Route & r) const {

    return villeA_ == r.villeA_ && villeB_ == r.villeB_ && distance_ == r.distance_;
}

bool Route::operator!=(const Route & r) const {

    return !(villeA_== r.villeA_ && villeB_ == r.villeB_ && distance_ == r.distance_);
}

