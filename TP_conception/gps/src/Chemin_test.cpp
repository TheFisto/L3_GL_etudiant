#include <CppUTest/CommandLineTestRunner.h>

#include "Chemin.hpp"
#include <fstream>
#include <sstream>

/// \brief implémentation interne pour les tests unitaires sur membres privés
/// 
class Chemin_test_ : public Chemin {

    public:

        Chemin_test_() = default;
        Chemin_test_(const Chemin_test_ & c) = default;
        Chemin_test_(const Chemin & c) : Chemin(c) {}

        void importerCsv_0() {
            Chemin_test_ c;
            std::ifstream infile("test1.csv");
            c.importerCsv(infile);
            CHECK_EQUAL("toto", c.routes_.back().villeA_);
        }

        void importerCsv_1(){
            Chemin_test_ c;
            std::ifstream infile("test2.csv");
            c.importerCsv(infile);
            CHECK_EQUAL("titi", c.routes_.back().villeA_);
            CHECK_EQUAL("tata", c.routes_.back().villeB_);
            CHECK_EQUAL(3, c.routes_.back().distance_);
        }

        void importerCsv_2(){
            Chemin_test_ c;
            std::ifstream infile("test3.csv");
            c.importerCsv(infile);
            CHECK_EQUAL("toto", c.routes_[1].villeA_);
            CHECK_EQUAL("tutu", c.routes_[1].villeB_);
            CHECK_EQUAL(12, c.routes_[1].distance_);
        }

        void importerCsv_3(){
            Chemin_test_ c;
            std::ifstream infile("testERR.csv");
            c.importerCsv(infile);
            CHECK_EQUAL(0,c.routes_.size());
        }

        void exporterDot_0() { 
            try {
                Chemin_test_ c;
                std::ostringstream oss;
                c.exporterDot(oss, "toto", "tata");
                FAIL("failed to throw exception");
            }
            catch (const std::string & msg) {
                CHECK_EQUAL("Chemin::calculerPlusCourt : routes_.empty()", msg);
            }
        }


};

TEST_GROUP(GroupChemin) { };
TEST(GroupChemin, importerCsv_0) { Chemin_test_().importerCsv_0(); }
TEST(GroupChemin, importerCsv_1) { Chemin_test_().importerCsv_1(); }
TEST(GroupChemin, importerCsv_2) { Chemin_test_().importerCsv_2(); }
//TEST(GroupChemin, importerCsv_3) { Chemin_test_().importerCsv_3(); }
TEST(GroupChemin, exporterDot_0) { Chemin_test_().exporterDot_0(); }

