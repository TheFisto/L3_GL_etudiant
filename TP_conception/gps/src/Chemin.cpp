#include "Chemin.hpp"
#include <limits>
#include <queue>
#include <sstream>

using namespace std;

/// \brief implémentation interne pour le calcul de plus court chemin
/// 
struct Parcours_ {
    Chemin cheminParcouru_;
    Chemin cheminRestant_;
    int distanceParcourue_;
};

Chemin Chemin::calculerPlusCourt(const string & ville1, 
        const string & ville2) const {

    if (routes_.empty())
        throw string("Chemin::calculerPlusCourt : routes_.empty()");

    if (ville1 == ville2)
        throw string("Chemin::calculerPlusCourt : ville1 == ville2");

    Chemin meilleurChemin;
    int meilleureDistance = std::numeric_limits<int>::max();
    queue<Parcours_> fileParcours;
    // initialise la file de parcours
    {
        Chemin cheminAvec, cheminSans;
        partitionner(ville1, cheminAvec, cheminSans);
        for (const Route & r : cheminAvec.routes_) {
            Chemin c;
            c.routes_.push_back(r);
            fileParcours.push(Parcours_{c, cheminSans, r.distance_});
        }
    }

    // teste tous les parcours
    while (not fileParcours.empty()) {
        const Parcours_ & parcoursCourant = fileParcours.front();
        const string & villeEtape 
            = parcoursCourant.cheminParcouru_.routes_.back().villeB_;
        if (villeEtape == ville2 
                and parcoursCourant.distanceParcourue_ < meilleureDistance) {
            meilleurChemin = parcoursCourant.cheminParcouru_;
            meilleureDistance = parcoursCourant.distanceParcourue_;
        }
        else if (villeEtape != ville1) {
            Chemin cheminAvec, cheminSans;
            const Chemin & cheminRestant = parcoursCourant.cheminRestant_;
            cheminRestant.partitionner(villeEtape, cheminAvec, cheminSans);
            for (const Route & r : cheminAvec.routes_) {
                Chemin c(parcoursCourant.cheminParcouru_);
                c.routes_.push_back(r);
                int d = parcoursCourant.distanceParcourue_ + r.distance_;
                fileParcours.push(Parcours_{c, cheminSans, d});
            }
        }
        fileParcours.pop();
    }

    return meilleurChemin;
}



void Chemin::partitionner(const string & ville, Chemin & cheminAvec, 
        Chemin & cheminSans) const {
/*
    for(Route r : routes_){
        if(r.villeA_ == ville || r.villeB_ == ville)
            cheminAvec.routes_.push_back(r);
        else
            cheminSans.routes_.push_back(r);
    }
*/
    //TODO
}

void Chemin::importerCsv(istream & is) {
    if(is.eof())
        return;
    std::string ligne, v1, v2;
    int d;
    Route route;
    do{
        std::getline(is, ligne, '\n');
        std::stringstream ss (ligne);
        std::getline(ss,v1,' ');
        std::getline(ss,v2,' ');
        ss >> d;
        route.villeA_=v1;
        route.villeB_=v2;
        route.distance_=d;
        if(d>0 && v1!="" && v2!=""){
            routes_.push_back(route);
        }
    }while(!is.eof());

}

void Chemin::exporterDot(ostream & os, const string & ville1, 
        const string & ville2) const {
    os << "graph {\n\tsplines=line;\n\t";
    try {
        Chemin PCC = calculerPlusCourt(ville1, ville2);
        for(Route r : PCC.routes_){
            os<<r.villeA_;
            if(r.villeA_ != ville2)
                os << " -- ";
        }
        os << " [color=red, penwidth=3];\n";
    }
    catch(const exception & e){
        throw e;
    }


    for(Route r : routes_){
        os << "\t" << r.villeA_ << " -- " << r.villeB_ << " [label=" << r.distance_ << "];\n";
    }
    os << "}\n";
}

