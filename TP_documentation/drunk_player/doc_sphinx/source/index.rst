.. drunk_player documentation master file, created by
   sphinx-quickstart on Wed Mar 27 11:23:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Drunk_player_cli
================

Description
-----------

Drunk_player est un système de lecture vidéos qui a trop bu. Il lit les vidéos contenues 
dans un dossier par morceaux, aléatoirement et parfois en transformant l'image.

Utilisation
-----------

.. code-block:: cpp

	usage: ./drunk_player_cli.out <folder> <output file>
	
Exemple
-------

.. code-block:: cpp

	$ ./drunk_player_cli.out ../data/ drunk_video.avi
	elapsed time: 5.444s
	
.. image:: ../../drunk_player_gui.png
