#include <math.h>
#include <iostream>

float fsinus(float x, float a, float b){
	return sin(2*M_PI*(a*x+b));
}

#include <pybind11/pybind11.h>
PYBIND11_PLUGIN(sinus){
	pybind11::module m("sinus");
	m.def("fsinus",&fsinus);
	return m.ptr();
}


