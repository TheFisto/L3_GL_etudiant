
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sinus

xs = range(10)
ys = [sinus.fSinus(x,2,0.25) for x in xs]
plt.plot(xs, ys)
plt.xlabel('x')
plt.ylabel('fSinus(x)')
plt.grid()
plt.savefig('sinus.png')
plt.clf()

